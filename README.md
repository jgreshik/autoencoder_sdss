## autoencoder_SDSS  

Autoencoder that trains and decodes SDSS images  
Training image data is currently uncompressed so it is ommitted from the repo

	don't try to run with encode value set to True, cannot train without image data

Run autoencoder.ipynb with params set in file  
Parameters are:

		normalize
		encode 
			for training with data in ./bin/data/
			models is saved in ./bin/model/
		decode 
			for sending images through autoencoder
			model is loaded from ./bin/model/
			outputs saved in ./bin/photo/
			currently the only model that is giving favorable outputs is tagged with 'norm'
		tag
			string for discerning between models saved/loaded, saved image outputs and logs
A log file for each model is saved in ./bin/  
Sometimes training new models results in only one output regardless of input ?  
Decoding currently works for 'norm' tag saved model  
Each model is only trained on 1500 images at <200 epochs in the interest of time, will be adding much more to training set in near future as well as increasing epochs
